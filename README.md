## aws helper ##

This is a simple RestFul Api exposing some AWS entities like ec2 and rds instances.
Also, a simple Bootstrap UI is provided that helps group information.
This can be extended to merge information from other tools like Marathon, Consul, Chronos, Jenkins etc...

### Build ###

mvn clean install

(Maven and Java 8 are required)

### Execute ###

`mvn spring-boot:run`

To run in 'localhost' mode for testing purposes use:

`mvn -Dspring.profiles.active=localhost spring-boot:run`

### Restful API ###

http://localhost:8090/

### Simple Web UI ###

Open url:

http://localhost:8090/tester/

### Configuration ###

In the application.yml you need to override the `aws.key.access` and `aws.key.secret`.
