package com.panos.awshelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.security.GeneralSecurityException;
import java.util.concurrent.Callable;

import static springfox.documentation.builders.PathSelectors.regex;

@SpringBootApplication
@EnableSwagger2
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    @ConditionalOnProperty("swagger.enabled")
    public Docket swaggerSpringMvcPlugin() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("aws-helper")
                .apiInfo(new ApiInfo("aws-helper",
                        "AWS Helper", "", "", "", "", ""))
                .genericModelSubstitutes(Callable.class, ResponseEntity.class)
                .select()
                .paths(regex("/rest/.*")) // and by paths
                .build();
    }

    @Bean
    public RestTemplate restTemplate() throws GeneralSecurityException {
        return new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }

}
