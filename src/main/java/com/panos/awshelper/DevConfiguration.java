package com.panos.awshelper;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!localhost")
public class DevConfiguration {

    @Value("${aws.key.access}")
    private String accessKey;
    @Value("${aws.key.secret}")
    private String secretKey;

    @Bean
    public AmazonEC2 ec2() {
        return Region.getRegion(Regions.EU_WEST_1).createClient(AmazonEC2Client.class,
                new StaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), new ClientConfiguration());
    }

    @Bean
    public AmazonRDS rds() {
        return Region.getRegion(Regions.EU_WEST_1).createClient(AmazonRDSClient.class,
                new StaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), new ClientConfiguration());
    }

    @Bean
    public AmazonS3 s3(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return Region.getRegion(Regions.EU_WEST_1).createClient(AmazonS3Client.class,
                new StaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), new ClientConfiguration());
    }

    @Bean
    public AmazonSQS sqs(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return Region.getRegion(Regions.EU_WEST_1).createClient(AmazonSQSClient.class,
                new StaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), new ClientConfiguration());
    }

    @Bean
    public AmazonSNS sns(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return Region.getRegion(Regions.EU_WEST_1).createClient(AmazonSNSClient.class,
                new StaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)), new ClientConfiguration());
    }
}
