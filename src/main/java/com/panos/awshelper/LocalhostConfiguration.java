package com.panos.awshelper;


import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DeleteDBInstanceRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.amazonaws.services.rds.model.Endpoint;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.asList;

@Configuration
@Profile("localhost")
public class LocalhostConfiguration {


    @Bean
    public AmazonEC2 ec2() {
        return new AmazonEC2Client() {
            @Override
            public DescribeInstancesResult describeInstances(DescribeInstancesRequest describeInstancesRequest) {
                return new DescribeInstancesResult().withReservations(
                        asList(
                                new Reservation().withInstances(
                                        asList(

                                                new Instance() {{
                                                    setInstanceId("1");
                                                    setPrivateIpAddress("1.2.14.2");
                                                    setLaunchTime(Date.from(LocalDateTime.now().minusDays(4).toInstant(UTC)));
                                                    setTags(asList(new Tag("ENV_ID", "panos"), new Tag("test", "a")));
                                                    setInstanceType(InstanceType.T2Micro);
                                                    setState(new InstanceState() {{
                                                        setCode(16);
                                                    }});
                                                    setInstanceLifecycle(InstanceLifecycleType.Spot);
                                                }},
                                                new Instance() {{
                                                    setInstanceId("2");
                                                    setLaunchTime(Date.from(LocalDateTime.now().minusDays(3).toInstant(UTC)));
                                                    setPrivateIpAddress("31.2.14.22");
                                                    setTags(asList(new Tag("ENV_ID", "panos"), new Tag("test", "a")));
                                                    setInstanceType(InstanceType.M4Xlarge);
                                                    setState(new InstanceState() {{
                                                        setCode(16);
                                                    }});
                                                }},
                                                new Instance() {{
                                                    setLaunchTime(Date.from(LocalDateTime.now().minusDays(2).toInstant(UTC)));
                                                    setInstanceId("3");
                                                    setPrivateIpAddress("31.2.14.122");
                                                    setTags(asList(new Tag("ENV_ID", "test"), new Tag("test", "a")));
                                                    setInstanceType(InstanceType.M4Large);
                                                    setState(new InstanceState() {{
                                                        setCode(16);
                                                    }});
                                                }}
                                        )
                                )
                        )
                );
            }
        };
    }


    @Bean
    public AmazonRDS rds() {
        return new AmazonRDSClient() {
            @Override
            public DescribeDBInstancesResult describeDBInstances() {
                return new DescribeDBInstancesResult() {{
                    setDBInstances(asList(
                            new DBInstance() {{
                                setDBName("panosDB1");
                                setDBInstanceIdentifier("panosId1");
                                setDBInstanceClass("db.t2.medium");
                                setEngine("mysql");
                                setDBInstanceStatus("deleting");
                                setEndpoint(new Endpoint() {{
                                    setAddress("db1.co2clknseqbw.eu-west-1.rds.amazonaws.com");
                                    setPort(3306);
                                }});
                                setInstanceCreateTime(Date.from(LocalDateTime.now().minusDays(2).toInstant(UTC)));
                            }},
                            new DBInstance() {{
                                setDBName("panosDB2");
                                setDBInstanceIdentifier("panosId2");
                                setDBInstanceClass("db.t2.medium");
                                setEngine("mysql");
                                setDBInstanceStatus("available");
                                setEndpoint(new Endpoint() {{
                                    setAddress("db2.co2clknseqbw.eu-west-1.rds.amazonaws.com");
                                    setPort(3306);
                                }});

                                setInstanceCreateTime(Date.from(LocalDateTime.now().minusDays(4).toInstant(UTC)));

                            }}
                    ));
                }};
            }

            @Override
            public DBInstance deleteDBInstance(DeleteDBInstanceRequest deleteDBInstanceRequest) {
                System.out.println("deleting " + deleteDBInstanceRequest.getDBInstanceIdentifier());
                return new DBInstance();
            }
        };
    }


    @Bean
    public AmazonS3 s3(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return new AmazonS3Client() {
            @Override
            public List<Bucket> listBuckets() throws AmazonClientException, AmazonServiceException {
                return Arrays.asList(
                        new Bucket("bucket1") {{
                            setCreationDate(Date.from(LocalDateTime.now().minusDays(4).toInstant(UTC)));
                        }},
                        new Bucket("bucket2") {{
                            setCreationDate(Date.from(LocalDateTime.now().minusDays(2).toInstant(UTC)));
                        }}
                );
            }

            @Override
            public ObjectListing listObjects(String bucketName) throws AmazonClientException, AmazonServiceException {
                return new ObjectListing() {
                    @Override
                    public List<S3ObjectSummary> getObjectSummaries() {
                        return asList(
                                new S3ObjectSummary() {{
                                    setBucketName("b1");
                                    setKey("o1");
                                }},
                                new S3ObjectSummary() {{
                                    setBucketName("b1");
                                    setKey("o2");
                                }}
                        );
                    }
                };
            }
        };
    }

    @Bean
    public AmazonSQS sqs(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return new AmazonSQSClient() {
            @Override
            public ListQueuesResult listQueues() {
                return new ListQueuesResult() {{
                    setQueueUrls(asList(
                            "http://32848234/queue1",
                            "http://32848234/queue2"
                    ));
                }};
            }

            @Override
            public ReceiveMessageResult receiveMessage(ReceiveMessageRequest receiveMessageRequest) {
                return new ReceiveMessageResult() {{
                    setMessages(asList(
                            new Message() {{
                                setBody("m1");
                            }},
                            new Message() {{
                                setBody("m2");
                            }}
                    ));
                }};
            }

            @Override
            public ReceiveMessageResult receiveMessage(String queueUrl) {
                return new ReceiveMessageResult() {{
                    setMessages(asList(
                            new Message() {{
                                setBody("m1");
                            }},
                            new Message() {{
                                setBody("m2");
                            }}
                    ));
                }};
            }

            @Override
            public GetQueueAttributesResult getQueueAttributes(String queueUrl, List<String> attributeNames) {
                return new GetQueueAttributesResult()
                        .addAttributesEntry("k1", "v1")
                        .addAttributesEntry("k2", "v2")
                        ;
            }

            @Override
            public void deleteQueue(String queueUrl) {
                System.out.println("Deleting " + queueUrl);
            }
        };
    }

    @Bean
    public AmazonSNS sns(@Value("${aws.key.access}") final String accessKey, @Value("${aws.key.secret}") final String secretKey) {
        return new AmazonSNSClient() {
            @Override
            public ListTopicsResult listTopics() {
                return new ListTopicsResult() {{
                    setTopics(asList(
                            new Topic() {{
                                setTopicArn("topicArn1");
                            }}, new Topic() {{
                                setTopicArn("topicArn2");
                            }}
                    ));
                }};
            }

            @Override
            public ListSubscriptionsResult listSubscriptions() {
                return new ListSubscriptionsResult() {{
                    setSubscriptions(asList(
                            new Subscription() {{

                                setEndpoint("arn:aws:sqs:eu-west-1:313443323640:queue1");
                            }}, new Subscription() {{
                                setEndpoint("arn:aws:sqs:eu-west-1:313443323640:queue2");
                            }}
                    ));
                }};
            }

            @Override
            public ListSubscriptionsByTopicResult listSubscriptionsByTopic(String topicArn) {
                if (topicArn.equalsIgnoreCase("topicArn1")) {
                    return new ListSubscriptionsByTopicResult() {{
                        setSubscriptions(asList(
                                new Subscription() {{
                                    setEndpoint("arn:aws:sqs:eu-west-1:313443323640:queue1");
                                }}
                        ));
                    }};
                } else {
                    return new ListSubscriptionsByTopicResult() {{
                        setSubscriptions(asList(
                                new Subscription() {{
                                    setEndpoint("arn:aws:sqs:eu-west-1:313443323640:queue2");
                                }},
                                new Subscription() {{
                                    setEndpoint("arn:aws:sqs:eu-west-1:313443323640:queue3");
                                }}
                        ));
                    }};

                }
            }

            @Override
            public void unsubscribe(String subscriptionArn) {
                System.out.println("unsubscribing " + subscriptionArn);
            }

            @Override
            public void deleteTopic(String topicArn) {
                System.out.println("deleting " + topicArn);
            }
        };
    }
}

