package com.panos.awshelper;

import com.amazonaws.services.sns.model.Subscription;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.assertj.core.api.Assertions.assertThat;

public class Utils {

    public static <T> Predicate<T> p(Predicate<T> p) {
        return p;
    }

    public static <T, R> Function<T, R> f(Function<T, R> f) {
        return f;
    }

    public static <T> Comparator<T> c(Comparator<T> c) {
        return c;
    }

    public static BigDecimal calculateCostOfInstance(BigDecimal costPerHour, Instant launchTime) {
        long hoursUpTime = Duration.between(launchTime, Instant.now()).toHours();
        return costPerHour.multiply(new BigDecimal(hoursUpTime));
    }

    public static BigDecimal calculateDailyCostOfInstance(BigDecimal costPerHour) {
        return costPerHour.multiply(new BigDecimal(24));
    }

    public static String extractSqsQueueName(String subEndpoint) {
        return Arrays.stream(subEndpoint.split(":")).reduce((a, b) -> b).orElse("");
    }

    public static String extractSqsRegion(String subEndpoint) {
        return subEndpoint.split(":")[3];
    }

    public static String buildQueueUrl(Subscription subscription) {
        String endpoint = subscription.getEndpoint();
        return "https://sqs." + extractSqsRegion(endpoint) + ".amazonaws.com/" + subscription.getOwner() + "/" + extractSqsQueueName(endpoint);
    }


}
