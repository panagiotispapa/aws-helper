package com.panos.awshelper.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

public class InstancePrice {
    private String type;
    private BigDecimal price;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public static Function<String, BigDecimal> buildPricingProvider(List<InstancePrice> prices) {
        Map<String, BigDecimal> priceMap = prices.stream().collect(toMap(InstancePrice::getType, InstancePrice::getPrice));
        return s -> priceMap.getOrDefault(s, BigDecimal.ZERO);
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
