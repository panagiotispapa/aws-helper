package com.panos.awshelper.model.ec2;

import java.math.BigDecimal;
import java.util.List;

public class EnvironmentDetails {
    private final String envId;
    private final List<InstanceDetails> instancesDetails;
    private final BigDecimal cost;
    private final BigDecimal dailyCost;

    public EnvironmentDetails(String envId, List<InstanceDetails> instancesDetails) {
        this.envId = envId;
        this.instancesDetails = instancesDetails;
        cost = instancesDetails.stream().map(InstanceDetails::getCost).reduce(BigDecimal.ZERO, BigDecimal::add);
        dailyCost = instancesDetails.stream().map(InstanceDetails::getDailyCost).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public String getEnvId() {
        return envId;
    }

    public List<InstanceDetails> getInstancesDetails() {
        return instancesDetails;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public BigDecimal getDailyCost() {
        return dailyCost;
    }
}
