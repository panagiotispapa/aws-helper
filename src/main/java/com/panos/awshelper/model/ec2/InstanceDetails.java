package com.panos.awshelper.model.ec2;

import com.amazonaws.services.ec2.model.Tag;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

public class InstanceDetails {

    private final String id;
    private final Instant launchTime;
    private final String ipAddress;
    private final String envId;
    private final String instanceType;
    private final String lifecycle;
    private final Integer stateCode;
    private final BigDecimal cost;
    private final BigDecimal dailyCost;
    private final List<Tag> tags;

    public InstanceDetails(String id, Instant launchTime, String ipAddress, String envId, String instanceType, String lifecycle, Integer stateCode, BigDecimal cost, BigDecimal dailyCost, List<Tag> tags) {
        this.id = id;
        this.launchTime = launchTime;
        this.ipAddress = ipAddress;
        this.envId = envId;
        this.instanceType = instanceType;
        this.lifecycle = lifecycle;
        this.stateCode = stateCode;
        this.cost = cost;
        this.dailyCost = dailyCost;
        this.tags = tags;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public String getId() {
        return id;
    }

    public Instant getLaunchTime() {
        return launchTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getEnvId() {
        return envId;
    }

    public String getInstanceType() {
        return instanceType;
    }

    public String getLifecycle() {
        return lifecycle;
    }

    public Integer getStateCode() {
        return stateCode;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public BigDecimal getDailyCost() {
        return dailyCost;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
