package com.panos.awshelper.model.rds;

import com.amazonaws.services.rds.model.Endpoint;

import java.math.BigDecimal;
import java.util.Date;

public class DBInstanceDetails {

    private final String dBInstanceIdentifier;
    private final String dBInstanceClass;
    private final Date createTime;
    private final Endpoint endpoint;
    private final String engine;
    private final String status;
    private final BigDecimal cost;
    private final BigDecimal dailyCost;

    public DBInstanceDetails(String dBInstanceIdentifier, String dBInstanceClass, Date createTime, Endpoint endpoint, String engine, String status, BigDecimal cost, BigDecimal dailyCost) {
        this.dBInstanceIdentifier = dBInstanceIdentifier;
        this.dBInstanceClass = dBInstanceClass;
        this.createTime = createTime;
        this.endpoint = endpoint;
        this.engine = engine;
        this.status = status;
        this.cost = cost;
        this.dailyCost = dailyCost;
    }

    public String getdBInstanceIdentifier() {
        return dBInstanceIdentifier;
    }

    public String getdBInstanceClass() {
        return dBInstanceClass;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public String getStatus() {
        return status;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public String getEngine() {
        return engine;
    }

    public BigDecimal getDailyCost() {
        return dailyCost;
    }
}
