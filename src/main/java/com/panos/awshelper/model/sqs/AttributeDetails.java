package com.panos.awshelper.model.sqs;

public class AttributeDetails {
    private String name;
    private String value;

    public AttributeDetails(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
