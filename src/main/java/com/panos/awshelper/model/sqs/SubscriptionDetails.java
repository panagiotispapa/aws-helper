package com.panos.awshelper.model.sqs;

import com.amazonaws.services.sns.model.Subscription;

import static com.panos.awshelper.Utils.buildQueueUrl;
import static com.panos.awshelper.Utils.extractSqsQueueName;

public class SubscriptionDetails {
    private String name;
    private String endpoint;
    private String queueUrl;


    public static SubscriptionDetails build(Subscription subscription) {
        return new SubscriptionDetails(
                extractSqsQueueName(subscription.getEndpoint()),
                subscription.getEndpoint(),
                buildQueueUrl(subscription)
        );
    }

    public SubscriptionDetails(String name, String endpoint, String queueUrl) {
        this.name = name;
        this.endpoint = endpoint;
        this.queueUrl = queueUrl;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public String getQueueUrl() {
        return queueUrl;
    }

    public String getName() {
        return name;
    }
}
