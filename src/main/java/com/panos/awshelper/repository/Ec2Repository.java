package com.panos.awshelper.repository;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public class Ec2Repository {

    private final AmazonEC2 client;
    private final String envIdTag;

    @Autowired
    public Ec2Repository(AmazonEC2 client, @Value("${ec2.envIdTag}") String envIdTag) {
        this.client = client;
        this.envIdTag = envIdTag;
    }

    public Stream<Instance> fetchRunningInstances() {
        return getInstances()
                .filter(i -> i.getState().getCode() == 16); // only running environments

    }


    private Stream<Instance> getInstances() {
        return client.describeInstances(new DescribeInstancesRequest().withFilters(new Filter().withName("tag-key").withValues(envIdTag)))
                .getReservations().stream()
                .map(Reservation::getInstances).flatMap(List::stream);
    }

}
