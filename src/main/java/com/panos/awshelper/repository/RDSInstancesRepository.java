package com.panos.awshelper.repository;

import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DeleteDBInstanceRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Repository
public class RDSInstancesRepository {
    private final AmazonRDS client;

    @Autowired
    public RDSInstancesRepository(AmazonRDS client) {
        this.client = client;
    }

    public List<DBInstance> fetchRdsInstanceDetails() {
        return client.describeDBInstances().getDBInstances();

//        List<DBInstance> allDBInstances = newArrayList();
//        describeDBInstancesResult(null, allDBInstances);
//        return allDBInstances;
    }

    public DBInstance deleteDBInstance(String dBInstanceIdentifier) {
        return client.deleteDBInstance(new DeleteDBInstanceRequest(dBInstanceIdentifier).withSkipFinalSnapshot(true));
    }

    private void describeDBInstancesResult(String marker, List<DBInstance> allDBInstances) {
        DescribeDBInstancesResult describeDBInstancesResult = describeDBInstancesResult(marker);
        allDBInstances.addAll(describeDBInstancesResult.getDBInstances());
        if (describeDBInstancesResult.getMarker() == null) {
            if(allDBInstances.isEmpty()) {
                describeDBInstancesResult(describeDBInstancesResult.getMarker(), allDBInstances);
            }
        } else {
            describeDBInstancesResult(describeDBInstancesResult.getMarker(), allDBInstances);
        }
    }
    private DescribeDBInstancesResult describeDBInstancesResult(String marker) {
        if (marker != null) {
            return client.describeDBInstances(new DescribeDBInstancesRequest().withMarker(marker));
        } else {
            return client.describeDBInstances();
        }
    }
}
