package com.panos.awshelper.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class S3BucketsRepository {
    private final AmazonS3 client;

    @Autowired
    public S3BucketsRepository(AmazonS3 client) {
        this.client = client;
    }

    public List<S3ObjectSummary> listObjects(String bucket) {
        return client.listObjects(bucket).getObjectSummaries();
    }

    public String getObjectHttpLink(String bucket, String key) {
        S3Object object = client.getObject(bucket, key);
        return object.getObjectContent().getHttpRequest().getURI().toString();
    }

    public List<Bucket> listBuckets() {
        return client.listBuckets();
    }

    public void deleteBucket(String bucketName) {
        deleteAllObjects(bucketName);
        client.deleteBucket(bucketName);
    }


    private void deleteAllObjects(String bucketName) {
        final List<DeleteObjectsRequest.KeyVersion> keysToDelete = client.listObjects(bucketName).getObjectSummaries().stream().map(S3ObjectSummary::getKey).map(DeleteObjectsRequest.KeyVersion::new).collect(toList());
        client.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(keysToDelete));
        System.out.println("Deleted " + keysToDelete.size() + " objects from bucket " + bucketName);
    }

}
