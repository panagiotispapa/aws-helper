package com.panos.awshelper.repository;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.Subscription;
import com.amazonaws.services.sns.model.Topic;
import com.amazonaws.services.sqs.AmazonSQS;
import com.panos.awshelper.Utils;
import com.panos.awshelper.model.sqs.SubscriptionDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class SNSRepository {

    private final AmazonSNS client;
    private final AmazonSQS sqsClient;

    @Autowired
    public SNSRepository(AmazonSNS client, AmazonSQS sqsClient) {
        this.client = client;
        this.sqsClient = sqsClient;
    }

    public List<String> fetchTopics() {
        return client.listTopics().getTopics().stream().map(Topic::getTopicArn).collect(toList());
    }

    public List<Subscription> fetchSubscriptions() {
        return client.listSubscriptions().getSubscriptions();
    }

    public List<SubscriptionDetails> fetchSubscriptions(String topicArn) {
        return client.listSubscriptionsByTopic(topicArn).getSubscriptions().stream().map(SubscriptionDetails::build).collect(toList());
    }

    public void deleteTopic(String topicArn) {
        List<Subscription> subscriptions = client.listSubscriptionsByTopic(topicArn).getSubscriptions();
        subscriptions.stream().map(Subscription::getSubscriptionArn).forEach(client::unsubscribe);
        subscriptions.stream().map(Utils::buildQueueUrl).collect(toList()).forEach(sqsClient::deleteQueue);
        client.deleteTopic(topicArn);
    }
}
