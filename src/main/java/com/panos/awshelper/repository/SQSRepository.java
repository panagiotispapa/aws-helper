package com.panos.awshelper.repository;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.panos.awshelper.model.sqs.AttributeDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class SQSRepository {

    private final AmazonSQS client;

    @Autowired
    public SQSRepository(AmazonSQS client) {
        this.client = client;
    }

    public List<String> fetchQueuesUrls() {

        List<String> queueUrls = client.listQueues().getQueueUrls();
        return queueUrls;
    }

    public List<Message> fetchMessages(String queueUrl) {
        return client.receiveMessage(new ReceiveMessageRequest().withQueueUrl(queueUrl).withVisibilityTimeout(1)).getMessages();
    }

    public List<AttributeDetails> getAttributes(String queueUrl) {
        return client.getQueueAttributes(queueUrl, Arrays.asList("All")).getAttributes().entrySet().stream().map(e -> new AttributeDetails(e.getKey(), e.getValue())).collect(toList());
    }

}
