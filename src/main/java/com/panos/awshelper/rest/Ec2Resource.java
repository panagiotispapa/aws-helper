package com.panos.awshelper.rest;

import com.panos.awshelper.rest.model.EnvironmentDetailsView;
import com.panos.awshelper.service.Ec2Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/ec2")
@Api(value = "ec2", description = "Ec2 Instances")
public class Ec2Resource {

    private final Ec2Service ec2Service;
    private final RestMapper mapper;

    @Autowired
    public Ec2Resource(Ec2Service ec2Service, RestMapper mapper) {
        this.ec2Service = ec2Service;
        this.mapper = mapper;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/environments")
    @ApiOperation(value = "Get the EC2 instances")
    public ResponseEntity<List<EnvironmentDetailsView>> getRdsInstances() {
        return new ResponseEntity<>(
                mapper.mapAsList(ec2Service.fetchRunningEnvironments(), EnvironmentDetailsView.class),
                HttpStatus.OK);
    }
}
