package com.panos.awshelper.rest;

import com.panos.awshelper.model.rds.DBInstanceDetails;
import com.panos.awshelper.service.RdsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/rds")
@Api(value = "rds", description = "RDS Instances")
public class RDSResource {

    private final RdsService service;

    @Autowired
    public RDSResource(RdsService service) {
        this.service = service;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/instances")
    @ApiOperation(value = "Get the RDS instances")
    public ResponseEntity<List<DBInstanceDetails>> getRdsInstances() {
        return new ResponseEntity<>(
                service.fetchRdsInstanceDetails(),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/instances/{dbIdentifier}")
    @ApiOperation(value = "Delete an RDS instance")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteRdsInstance(@PathVariable("dbIdentifier") String dbIdentifier) {
        service.deleteDBInstance(dbIdentifier);
    }

}
