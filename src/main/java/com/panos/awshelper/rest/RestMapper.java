package com.panos.awshelper.rest;

import com.panos.awshelper.model.ec2.EnvironmentDetails;
import com.panos.awshelper.model.ec2.InstanceDetails;
import com.panos.awshelper.rest.model.EnvironmentDetailsView;
import com.panos.awshelper.rest.model.InstanceDetailsView;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class RestMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(EnvironmentDetails.class, EnvironmentDetailsView.class)
                .byDefault()
                .register();

        factory.classMap(InstanceDetails.class, InstanceDetailsView.class)
                .byDefault()
//                .field("id", "queueId")
                .register();

    }
}
