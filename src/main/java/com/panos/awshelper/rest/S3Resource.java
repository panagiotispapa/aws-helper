package com.panos.awshelper.rest;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.panos.awshelper.repository.S3BucketsRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/s3")
@Api(value = "s3", description = "S3 Buckets")
public class S3Resource {

    private final S3BucketsRepository repository;

    @Autowired
    public S3Resource(S3BucketsRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/buckets")
    @ApiOperation(value = "Get the S3 buckets")
    public ResponseEntity<List<Bucket>> getS3Buckets() {
        return new ResponseEntity<>(
                repository.listBuckets(),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/buckets/{bucket}")
    @ApiOperation(value = "Delete the S3 buckets")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteS3Bucket(@PathVariable("bucket") String bucket) {
        repository.deleteBucket(bucket);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/buckets/{bucket}/objects")
    @ApiOperation(value = "List objects in bucket")
    public List<S3ObjectSummary> listObjects(@PathVariable("bucket") String bucket) {
        return repository.listObjects(bucket);
    }


}
