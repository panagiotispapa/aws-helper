package com.panos.awshelper.rest;

import com.amazonaws.services.sns.model.Subscription;
import com.panos.awshelper.model.sqs.SubscriptionDetails;
import com.panos.awshelper.repository.SNSRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/rest/sns")
@Api(value = "sns", description = "Get SNS resources")
public class SNSResource {

    private final SNSRepository snsRepository;

    @Autowired
    public SNSResource(SNSRepository snsRepository) {
        this.snsRepository = snsRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/topics")
    @ApiOperation(value = "List topics")
    public ResponseEntity<List<String>> listTopics() {
        return new ResponseEntity<>(snsRepository.fetchTopics(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/subscriptions")
    @ApiOperation(value = "List subscriptions")
    public ResponseEntity<List<Subscription>> listSubscriptions() {
        return new ResponseEntity<>(snsRepository.fetchSubscriptions(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/topics/{topic}/subscriptions")
    @ApiOperation(value = "List subscriptions")
    public ResponseEntity<List<SubscriptionDetails>> listSubscriptions(@PathVariable("topic") String topic) throws UnsupportedEncodingException {
        return new ResponseEntity<>(snsRepository.fetchSubscriptions(topic), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/topics/{topic}")
    @ApiOperation(value = "Delete topic and all subscribed queues")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteTopicAndQueues(@PathVariable("topic") String topic) {
        snsRepository.deleteTopic(topic);
    }
}
