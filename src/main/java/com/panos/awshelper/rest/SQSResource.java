package com.panos.awshelper.rest;

import com.amazonaws.services.sqs.model.Message;
import com.panos.awshelper.model.sqs.AttributeDetails;
import com.panos.awshelper.repository.SQSRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static java.net.URLDecoder.decode;
import static java.nio.charset.StandardCharsets.UTF_8;

@RestController
@RequestMapping("/rest/sqs")
@Api(value = "sqs", description = "Get SQS resources")
public class SQSResource {

    private final SQSRepository sqsRepository;

    @Autowired
    public SQSResource(SQSRepository sqsRepository) {
        this.sqsRepository = sqsRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/queues")
    @ApiOperation(value = "List queues urls")
    public ResponseEntity<List<String>> listObjects() {
        return new ResponseEntity<>(sqsRepository.fetchQueuesUrls(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/queues/{queueUrl}/messages")
    @ApiOperation(value = "List queues messages")
    public ResponseEntity<List<Message>> listObjects(@PathVariable("queueUrl") String queueUrl) throws UnsupportedEncodingException {
        return new ResponseEntity<>(sqsRepository.fetchMessages(decode(queueUrl, UTF_8.name())), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/queues/{queueUrl}/attributes")
    @ApiOperation(value = "List queues attributes")
    public ResponseEntity<List<AttributeDetails>> listAttributes(@PathVariable("queueUrl") String queueUrl) throws UnsupportedEncodingException {
        return new ResponseEntity<>(sqsRepository.getAttributes(decode(queueUrl, UTF_8.name())), HttpStatus.OK);
    }

}
