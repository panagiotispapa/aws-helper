package com.panos.awshelper.rest.model;

import java.util.List;

public class EnvironmentDetailsView {
    private String envId;
    private List<InstanceDetailsView> instancesDetails;
    private String cost;
    private String dailyCost;

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public List<InstanceDetailsView> getInstancesDetails() {
        return instancesDetails;
    }

    public void setInstancesDetails(List<InstanceDetailsView> instancesDetails) {
        this.instancesDetails = instancesDetails;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDailyCost() {
        return dailyCost;
    }

    public void setDailyCost(String dailyCost) {
        this.dailyCost = dailyCost;
    }
}
