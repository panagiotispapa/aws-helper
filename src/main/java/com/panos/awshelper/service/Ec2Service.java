package com.panos.awshelper.service;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Tag;
import com.panos.awshelper.model.InstancePrice;
import com.panos.awshelper.model.ec2.EnvironmentDetails;
import com.panos.awshelper.model.ec2.InstanceDetails;
import com.panos.awshelper.repository.Ec2Repository;
import com.panos.awshelper.spring.config.AwsEc2PricingConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static com.panos.awshelper.Utils.calculateCostOfInstance;
import static com.panos.awshelper.Utils.calculateDailyCostOfInstance;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
public class Ec2Service {

    private final Ec2Repository ec2Repository;
    private final String envIdTag;
    private final Function<String, BigDecimal> costPerHourProvider;

    @Autowired
    public Ec2Service(Ec2Repository ec2Repository, @Value("${ec2.envIdTag}") String envIdTag, AwsEc2PricingConfiguration ec2PricingConfiguration) {
        this.ec2Repository = ec2Repository;
        this.envIdTag = envIdTag;

        costPerHourProvider = InstancePrice.buildPricingProvider(ec2PricingConfiguration.getPricing());

    }

    public List<EnvironmentDetails> fetchRunningEnvironments() {
        return ec2Repository.fetchRunningInstances()
                .filter(i -> i.getState().getCode() == 16) // only running environments
                .map(toInstanceDetails(costPerHourProvider))
                .collect(groupingBy(InstanceDetails::getEnvId))
                .entrySet().stream().map(e -> new EnvironmentDetails(e.getKey(), e.getValue())).collect(toList());
    }

    public Function<Instance, InstanceDetails> toInstanceDetails(Function<String, BigDecimal> costPerHourProvider) {
        return instance -> {
            BigDecimal hourlyCost = costPerHourProvider.apply(instance.getInstanceType());
            return new InstanceDetails(
                    instance.getInstanceId(),
                    instance.getLaunchTime().toInstant(),
                    instance.getPrivateIpAddress(),
                    getTagValue(instance, envIdTag).orElse("-"),
                    instance.getInstanceType(),
                    parseLifecycle(instance),
                    instance.getState().getCode(),
                    calculateCostOfInstance(hourlyCost, instance.getLaunchTime().toInstant()),
                    calculateDailyCostOfInstance(hourlyCost),
                    instance.getTags());
        };

    }

    private static Optional<String> getTagValue(Instance instance, String tag) {
        return instance.getTags().stream().filter(t -> Objects.equals(tag, t.getKey())).findFirst().map(Tag::getValue);
    }

    private static String parseLifecycle(Instance instance) {
        return Optional.ofNullable(instance.getInstanceLifecycle()).orElse("-");
    }
}
