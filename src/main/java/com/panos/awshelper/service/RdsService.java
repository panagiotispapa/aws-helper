package com.panos.awshelper.service;

import com.amazonaws.services.rds.model.DBInstance;
import com.panos.awshelper.model.InstancePrice;
import com.panos.awshelper.model.rds.DBInstanceDetails;
import com.panos.awshelper.repository.RDSInstancesRepository;
import com.panos.awshelper.spring.config.AwsRdsPricingConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

import static com.panos.awshelper.Utils.calculateCostOfInstance;
import static com.panos.awshelper.Utils.calculateDailyCostOfInstance;
import static java.util.stream.Collectors.toList;

@Service
public class RdsService {

    private RDSInstancesRepository repository;
    private final Function<String, BigDecimal> costPerHourProvider;

    @Autowired
    public RdsService(RDSInstancesRepository repository, AwsRdsPricingConfiguration pricingConfiguration) {
        this.repository = repository;
        costPerHourProvider = InstancePrice.buildPricingProvider(pricingConfiguration.getPricing());

    }

    public List<DBInstanceDetails> fetchRdsInstanceDetails() {
        return repository.fetchRdsInstanceDetails().stream().map(toInstanceDetails(costPerHourProvider)).collect(toList());
    }

    public void deleteDBInstance(String dBInstanceIdentifier) {
        repository.deleteDBInstance(dBInstanceIdentifier);
    }

    public Function<DBInstance, DBInstanceDetails> toInstanceDetails(Function<String, BigDecimal> costPerHourProvider) {
        return instance -> {
            BigDecimal hourlyCost = costPerHourProvider.apply(instance.getDBInstanceClass());
            return new DBInstanceDetails(
                    instance.getDBInstanceIdentifier(),
                    instance.getDBInstanceClass(),
                    instance.getInstanceCreateTime(),
                    instance.getEndpoint(),
                    instance.getEngine(),
                    instance.getDBInstanceStatus(),
                    calculateCostOfInstance(hourlyCost, instance.getInstanceCreateTime().toInstant()),
                    calculateDailyCostOfInstance(hourlyCost));
        };
    }

}
