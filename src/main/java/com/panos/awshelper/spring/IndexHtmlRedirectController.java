package com.panos.awshelper.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexHtmlRedirectController {

    @RequestMapping("/")
    public String redirectToSwagger() {
        return "redirect:swagger-ui.html";
    }

    @RequestMapping("/tester")
    public String redirectToTester() {
        return "redirect:/tester/environments.html";
    }

}
