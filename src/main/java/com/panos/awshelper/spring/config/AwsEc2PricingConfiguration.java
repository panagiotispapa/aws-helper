package com.panos.awshelper.spring.config;

import com.panos.awshelper.model.InstancePrice;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("aws.ec2")
public class AwsEc2PricingConfiguration {
    private List<InstancePrice> pricing;

    public List<InstancePrice> getPricing() {
        return pricing;
    }

    public void setPricing(List<InstancePrice> pricing) {
        this.pricing = pricing;
    }

}
