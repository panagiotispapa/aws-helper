var ec2Service = {
    getEnvironments: function (callback) {
        performGet("/rest/ec2/environments", callback);
    }
};

var rdsService = {
    getRDSInstances: function (callback) {
        performGet("/rest/rds/instances", callback);
    },

    deleteRdsInstance: function (dbIdentifier, callback) {
        performDelete("/rest/rds/instances/" + dbIdentifier, {}, callback);
    }
};

var s3Service = {
    getBuckets: function (callback) {
        performGet("/rest/s3/buckets", callback);
    },

    getObjects: function (bucket, callback) {
        performGet("/rest/s3/buckets/" + bucket + "/objects", callback);
    },

    deleteBucket: function (bucket, callback) {
        performDelete("/rest/s3/buckets/" + bucket, {}, callback);
    },

    listObjects: function (bucket, callback) {
        performGet("/rest/s3/buckets/" + bucket + "/objects", callback);
    }

};

var snsService = {
    fetchTopics: function (callback) {
        performGet("/rest/sns/topics/", callback);
    },

    fetchSubscriptions: function (callback) {
        performGet("/rest/sns/subscriptions/", callback);
    },

    fetchSubscriptionsForTopic: function (topic, callback) {
        performGet("/rest/sns/topics/" + topic + "/subscriptions/", callback);
    },

    deleteTopic: function (topic, callback) {
        performDelete("/rest/sns/topics/" + topic, {}, callback);
    }
};

var sqsService = {
    fetchQueues: function (callback) {
        performGet("/rest/sqs/queues/", callback);
    },

    fetchMessages: function (queue, callback) {
        performGet("/rest/sqs/queues/" + encode(queue) + "/messages", callback);
    },

    fetchAttributes: function (queue, callback) {
        performGet("/rest/sqs/queues/" + encode(queue) + "/attributes", callback);
    }
};


function performGet(url, callback) {
    $.get(url, callback);
}

function performPost(url, data, callback) {

    $.ajax({
        url: url,
        "type": "POST",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while posting to " + url);
    });
}

function performPut(url, data, callback) {

    $.ajax({
        url: url,
        "type": "PUT",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while posting to " + url);
    });
}

function performDelete(url, data, callback) {
    $.ajax({
        url: url,
        "type": "DELETE",
        "data": JSON.stringify(data),
        "contentType": "application/json"
    }).then(callback).fail(function () {
        alert("Error while deleting " + url);
    });
}

function encode(content) {
    return encodeURIComponent(encodeURIComponent(content));
}
