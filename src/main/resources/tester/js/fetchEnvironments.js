$(document).ready(function () {

    prepareHeader("Running environments");

    var dropdowns = [
        buildDropdown("linksDropDownList", "Links")
    ];


    addToBody([
            buildNavBar("AWS Helper", dropdowns),
            buildMainContainer()
        ]
    );

    addToMainContainer([
            buildH2("Running environments", "runningEnvTitle"),
            buildDiv("ec2InstancesTable")
        ]
    );


    ec2Service.getEnvironments(handleEnvironments);

    function handleEnvironments(environments) {

        addLengthAtTheEndOfTheTitle("runningEnvTitle", environments);

        appendTableWithRows(
            "#ec2InstancesTable",
            environments.map(parseEnvironmentDetails),
            ["envId", "Instances", "Cost"]
        );
    }

    function parseEnvironmentDetails(environmentDetails) {
        return [
            environmentDetails.envId,
            buildInstancesColumn(environmentDetails),
            placeVertically([
                "Total: " + formatCost(environmentDetails.cost),
                "Daily: " + formatCost(environmentDetails.dailyCost)
            ])
        ];
    }

    function buildInstancesColumn(environmentDetails) {
        return createCollapseElement(
            "instancesOf" + environmentDetails.envId,
            createTableWithRows(
                environmentDetails.instancesDetails.map(parseInstanceDetails),
                ["ip", "type", "tags", "launched/cost"]
            ),
            environmentDetails.instancesDetails.length + " instances"
        );

    }

    function parseInstanceDetails(instancesDetails) {
        return [
            instancesDetails.ipAddress,
            instancesDetails.instanceType,
            buildTagsColumn(instancesDetails),
            placeVertically([
                timeSince(Date.parse(instancesDetails.launchTime)) + " ago",
                "Total: " + formatCost(instancesDetails.cost),
                "Daily: " + formatCost(instancesDetails.dailyCost)
            ])

        ]
    }

    function formatCost(cost) {
        return "$" + cost;
    }

    function buildTagsColumn(instancesDetails) {
        return createCollapseElement(
            "tagsOf" + instancesDetails.id,
            createTableWithRows(
                instancesDetails.tags.map(tag => [
                    bold(tag.key + ": "),
                    tag.value
                ])
            ),
            "tags"
        );
    }
});
