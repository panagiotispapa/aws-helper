$(document).ready(function () {

    prepareHeader("RDS instances");

    var dropdowns = [
        buildDropdown("linksDropDownList", "Links")
    ];

    addToBody([
            buildNavBar("AWS Helper", dropdowns),
            buildMainContainer(),
            buildRDSDeleteModal()
        ]
    );


    addToMainContainer([
        buildH2("RDS instances", "rdsTitle"),
        buildDiv("rdsInstancesTable")
        ]
    );


    rdsService.getRDSInstances(handleInstances);

    function handleInstances(instances) {
        addLengthAtTheEndOfTheTitle("rdsTitle", instances);
        appendTableWithRows(
            "#rdsInstancesTable",
            instances.map(parseInstance),
            ["Instances", "Type/Status", "Cost", "Endpoint"]
        )


    }

    function parseInstance(instanceDetails) {
        return [
            placeVertically([
                instanceDetails.dBInstanceIdentifier,
                instanceDetails.dBInstanceClass,
                createDeleteRdsInstanceButton(instanceDetails)
            ]),
            placeVertically([
                instanceDetails.status,
                instanceDetails.engine
            ]),
            placeVertically([
                timeSince(Date.parse(instanceDetails.createTime)) + " ago",
                "Total: $" + instanceDetails.cost,
                "Daily: $" + instanceDetails.dailyCost
            ]),
            placeVertically(parseEndpoint(instanceDetails.endpoint), "", "", TABLE_STYLES.STRIPED)
        ];

    }

    function createDeleteRdsInstanceButton(instanceDetails) {
        var dbIdentifier = instanceDetails.dBInstanceIdentifier;
        return createButton("Delete", function () {
            var r = confirm("You are about to delete rds instance " + dbIdentifier);
            if (r == true) {
                console.log("deleting rds instance " + dbIdentifier);
                rdsService.deleteRdsInstance(dbIdentifier, function (data) {
                    //showKillModal("Service " + service + " has been killed");
                    showRdsDeleteModal(dbIdentifier);
                });
            } else {
            }

        }, BUTTON_STYLES.DANGER, instanceDetails.status != "available");
    }


    function parseEndpoint(endPoint) {
        return endPoint ?
            [endPoint.address, endPoint.port] :
            [];
    }

    function showRdsDeleteModal(dbIdentifier) {
        $('#rdsDeleteMsg').html("Deleted " + dbIdentifier);
        $('#refreshPageLink').attr("href", window.location.href);
        showModal('#rdsDeleteModal');
    }


});
