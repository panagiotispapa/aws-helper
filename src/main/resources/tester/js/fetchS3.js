$(document).ready(function () {

    prepareHeader("S3 instances");

    var dropdowns = [
        buildDropdown("linksDropDownList", "Links")
    ];

    addToBody([
            buildNavBar("AWS Helper", dropdowns),
            buildS3DeleteModal(),
            buildMainContainer()
        ]
    );

    addToMainContainer([
            buildH2("S3 instances", "s3Title"),
            buildDiv("s3BucketsTable")
        ]
    );

    s3Service.getBuckets(handleBuckets);

    function handleBuckets(buckets) {
        addLengthAtTheEndOfTheTitle("s3Title", buckets);

        appendTableWithRows(
            "#s3BucketsTable",
            buckets.map(parseBucket),
            ["Bucket Name", "Created", "Action"]
        )
    }

    function parseBucket(bucketDetails) {

        var bucketName = bucketDetails.name;
        return [
            buildHref("s3Objects.html?bucket=" + bucketName, bucketName),
            timeSince(Date.parse(bucketDetails.creationDate)) + " ago",
            createDeleteBucketButton(bucketName)
        ];
    }

    function createDeleteBucketButton(bucketName) {
        return createButton("Delete", function () {
            var r = confirm("You are about to delete bucket " + bucketName);
            if (r == true) {
                console.log("deleting bucket " + bucketName);
                s3Service.deleteBucket(bucketName, function (data) {

                });
                showS3DeleteModal(bucketName);
            } else {
            }

        }, BUTTON_STYLES.DANGER, false);
    }

    function buildS3DeleteModal() {
        return buildModal(
            "s3DeleteModal",
            buildModalHeader("Deleted"),
            buildModalBody(
                placeVerticallyUsingP([
                    buildDiv("s3DeleteMsg", null, ["Please refresh page."]),
                    buildActiveLink("refreshPageLink", "Refresh")
                ])
            )
        );
    }

    function showS3DeleteModal(bucketName) {
        $('#s3DeleteMsg').html("Deleted " + bucketName);
        $('#refreshPageLink').attr("href", window.location.href);
        showModal('#s3DeleteModal');
    }

});
