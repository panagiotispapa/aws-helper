$(document).ready(function () {
    var bucket = gup("bucket", document.location.href);


    //getById("bucketNameInForm").setAttribute("value", bucket);

    prepareHeader("S3 Objects");

    addToBody([
            buildMainContainer(),
            buildS3DeleteObjectModal()
        ]
    );

    addToMainContainer([
            buildH2("S3 Objects on " + bucket, "objectsTitle"),
            buildDiv("objectsTable")
        ]
    );

    s3Service.listObjects(bucket, handleObjects);

    function handleObjects(objects) {

        addLengthAtTheStartOfTheTitle("objectsTitle", objects);

        appendTableWithRows(
            "#objectsTable",
            objects.map(toRow),
            ["Key", "Size", "Since", "Actions"]
        );

    }

    function toRow(object) {
        //var previewId = object.bucketName + "_object.key_" + "link";
        //
        //s3Service.getObjectLink(object.bucketName, object.key, handleObject(previewId));
        return [
            buildHref("s3object.html?bucket=" + object.bucketName + "&key=" + object.key, object.key),
            //object.key,
            object.size,
            timeSince(Date.parse(object.lastModified)) + " ago",

            createDeleteObjectButton(object.bucketName, object.key)
        ];
    }


    function createDeleteObjectButton(bucketName, key) {
        return createButton("Delete", function () {
            var r = confirm("You are about to delete object " + key);
            if (r == true) {
                console.log("deleting object " + key);
                s3Service.deleteObject(bucketName, key, function (data) {
                    //showKillModal("Service " + service + " has been killed");
                });
                showS3DeleteObjectModal(key);
            } else {
            }

        }, BUTTON_STYLES.DANGER, false);
    }

    function buildS3DeleteObjectModal() {
        return buildModal(
            "s3DeleteObjectModal",
            buildModalHeader("Deleted"),
            buildModalBody(
                placeVerticallyUsingP([
                    buildDiv("s3DeleteObjectMsg", null, ["Please refresh page."]),
                    buildActiveLink("refreshPageLink", "Refresh")
                ])
            )
        );
    }

    function showS3DeleteObjectModal(key) {
        $('#s3DeleteObjectMsg').html("Deleted " + key);
        $('#refreshPageLink').attr("href", window.location.href);
        showModal('#s3DeleteObjectModal');
    }


});
