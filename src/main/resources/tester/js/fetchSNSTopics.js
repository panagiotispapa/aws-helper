$(document).ready(function () {
    //var env = gup("env", document.location.href);

    prepareHeader("SNS Topics");

    var dropdowns = [
        buildDropdown("linksDropDownList", "Links")
    ];

    addToBody([
            buildNavBar("QA Manager", dropdowns),
            buildSnsTopicDeleteModal(),
            buildMainContainer()
        ]
    );

    addToMainContainer([
            buildH2("SNS Topics", "snsTitle"),
            buildDiv("topicsTable")
            //buildH2("SNS Subscriptions"),
            //buildDivWith("subscriptionsTable")
        ]
    );

    snsService.fetchTopics(handleTopics);

    //snsService.fetchSubscriptions(handleSubscriptions);

    function handleTopics(topics) {

        addLengthAtTheEndOfTheTitle("snsTitle", topics);

        appendTableWithRows(
            "#topicsTable",
            topics.map(handleTopic),
            ["Topic", "Subscriptions", "Actions"]
        );

        topics.forEach(function (topic) {
            snsService.fetchSubscriptionsForTopic(topic, handleSubscriptionsForTopic(topic));
        });
    }

    function handleSubscriptionsForTopic(topic) {
        return function (subscriptions) {
            appendTableWithRows(
                "#subOf" + topic,
                subscriptions.map(subscription =>
                    [
                        subscription.name,
                        buildDiv("messagesOf" + subscription.name),
                        buildDiv("attributesOf" + subscription.name)
                    ]
                ),
                "",
                TABLE_STYLES.CONDENSED
            );

            subscriptions.forEach(subscription => {
                sqsService.fetchMessages(subscription.queueUrl, handleQueueMessages(subscription));
                sqsService.fetchAttributes(subscription.queueUrl, handleQueueAttributes(subscription));
            });
        }
    }

    function handleQueueMessages(subscription) {
        var queueName = subscription.name;
        return messages =>
            $("#messagesOf" + queueName).append(
                createCollapseElement(
                    "collapsedMessagesOf" + queueName,
                    createTableWithRows(
                        messages.map(message => message.body)
                    ),
                    messages.length + " messages"
                )
            )
    }

    function handleQueueAttributes(subscription) {
        var queueName = subscription.name;
        return attributes =>
            $("#attributesOf" + queueName).append(
                createCollapseElement(
                    "collapsedAttributesOf" + queueName,
                    createTableWithRows(
                        attributes.map(attribute =>
                            [
                                attribute.name,
                                attribute.value
                            ]
                        ),
                        ["key", "value"]
                    ),
                    attributes.length + " attributes"
                )
            );
    }

    function handleTopic(topic) {
        return [
            getTopicName(topic),
            buildDiv("subOf" + topic),
            createDeleteTopicButton(topic)
        ];
    }

    function getTopicName(topic) {
        var parts = topic.split(":");
        return parts[parts.length - 1];
    }

    function createDeleteTopicButton(topic) {
        return createButton("Delete", function () {
            var r = confirm("You are about to delete topic " + topic);
            if (r == true) {
                console.log("deleting topic " + topic);
                snsService.deleteTopic(topic, function (data) {
                    showTopicDeleteModal(topic);
                });
            } else {
            }

        }, BUTTON_STYLES.DANGER, false);
    }

    function showTopicDeleteModal(topic) {
        $('#snsTopicDeleteMsg').html("Deleted " + topic);
        $('#refreshPageLink').attr("href", window.location.href);
        showModal('#snsTopicDeleteModal');
    }

});
