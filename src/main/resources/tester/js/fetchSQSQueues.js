$(document).ready(function () {

    prepareHeader("SQS Queues");

    var dropdowns = [
        buildDropdown("linksDropDownList", "Links")
    ];

    addToBody([
            buildNavBar("QA Manager", dropdowns),
            buildMainContainer()
        ]
    );

    addToMainContainer([
            buildH2("SQS Queues", "sqsTitle"),
            buildDiv("sqsQueuesTable")
        ]
    );

    sqsService.fetchQueues(handleSQSQueues);

    function handleSQSQueues(queues) {

        addLengthAtTheEndOfTheTitle("sqsTitle", queues);

        appendTableWithRows(
            "#sqsQueuesTable",
            queues.map(queue =>
                [
                    queue,
                    //buildHref(queue, getQueueName(queue)),
                    buildDiv("messagesOf" + queue)
                ]
            ),
            ["Queue", "Messages"]
        );


        queues.forEach(queue =>
            sqsService.fetchMessages(queue, handleMessages(queue))
        )
    }

    function handleMessages(queue) {
        return messages =>
            $("#messagesOf" + queue).append(
                createCollapseElement(
                    "collapsedMessagesOf" + getQueueName(queue),
                    createTableWithRows(
                        messages.map(message => [message.body])
                    ),
                    messages.length + " messages"
                )
            );
    }

    function getQueueName(queue) {
        var parts = queue.split("/");
        return parts[parts.length - 1];
    }

});
