
function buildRDSDeleteModal() {
    return buildModal(
        "rdsDeleteModal",
        buildModalHeader("Deleted"),
        buildModalBody(
            placeVerticallyUsingP([
                buildDiv("rdsDeleteMsg", null, ["Please refresh page."]),
                buildActiveLink("refreshPageLink", "Refresh")
            ])
        )
    );
}

function buildSnsTopicDeleteModal() {
    return buildModal(
        "snsTopicDeleteModal",
        buildModalHeader("Deleted"),
        buildModalBody(
            placeVerticallyUsingP([
                buildDiv("snsTopicDeleteMsg", null, ["Please refresh page."]),
                buildActiveLink("refreshPageLink", "Refresh")
            ])
        )
    );
}


function buildModal(id, header, body) {
    var modalDiv = buildDiv(
        id,
        "modal fade",
        [buildModalDialog(header, body)]
    );
    modalDiv.role = "dialog";
    return modalDiv;

}

function buildModalDialog(header, body) {
    return buildDiv(
        "",
        "modal-dialog",
        [buildModalContent(header, body)]
    );
}

function buildModalContent(header, body) {
    return buildDiv(
        "",
        "modal-content",
        [header, body]
    );
}

function buildModalBody(content) {
    return buildDiv(
        "",
        "modal-body",
        [content]
    );
}
function buildModalHeader(titleTxt) {
    return buildDiv(
        "",
        "modal-header",
        [
            buildCloseButton(),
            buildModalTitle(titleTxt)
        ]);
}

function buildModalTitle(txt) {
    var h4 = document.createElement("h4");
    h4.className = "modal-title";
    h4.innerHTML = txt;
    return h4;
}

function buildActiveLink(id, txt, openInNewTab) {
    var activeLink = document.createElement("a");
    activeLink.id = id;
    activeLink.href = "#";
    if (openInNewTab) {
        activeLink.target = "_blank";
    }
    activeLink.role = "button";
    activeLink.className = "btn btn-primary btn-lg active";
    activeLink.innerHTML = txt;
    return activeLink;
}

function buildCloseButton() {
    var button = document.createElement("button");

    d3.select(button)
        .html("&times;")
        .classed("close", true)
        .attr("data-dismiss", "modal")
        .attr("type", "button");

    return button;
}

function showModal(modal) {
    $(modal).modal({
        backdrop: false,
        show: true
    });
}

function hideModal(modal) {
    $(modal).modal('hide');
}


