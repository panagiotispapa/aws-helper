function buildNavBar(headerTxt, dropdowns) {
    var nav = createElement("nav", "navBar", "navbar navbar-default navbar-fixed-top");
    nav.appendChild(buildNavBarContainer(headerTxt, dropdowns));
    return nav;
}

function buildNavBarContainer(headerTxt, dropdowns) {
    return buildDiv("", "container-fluid", [buildNavBarHeader(headerTxt), buildNavBarDropdownsBody(dropdowns)]);
}

function buildNavBarDropdownsBody(dropdowns) {
    return buildDiv("navBarDropdowns", null, dropdowns);
}

function buildNavBarHeader(txt) {
    var link = createElement("a", "", "navbar-brand");
    link.href = "#";
    link.innerHTML = txt;
    return link;
}

function buildDropdown(id, txt) {

    var dropDownMenu = createElement("ul", id, "dropdown-menu");
    var link = createElement("a", "", "dropdown-toggle");
    link.href = "#";
    link.setAttribute("data-toggle", "dropdown");
    link.innerHTML = txt + ' <b class="caret"></b>';

    var dropdown = createElement("li", "", "dropdown");
    $(dropdown).append([link, dropDownMenu]);

    var navBarNav = createElement("ul", "", "nav navbar-nav");
    navBarNav.appendChild(dropdown);

    return navBarNav;
}