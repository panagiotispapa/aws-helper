$(document).ready(function () {

    var links = [
        buildHrefObj("environments.html", "EC2", false),
        buildHrefObj("rds.html", "RDS", false),
        buildHrefObj("s3.html", "S3", false),
        buildHrefObj("snsTopics.html", "SNS", false),
        buildHrefObj("sqsQueues.html", "SQS", false)
    ];

    populateDropDownListWithLinks(
        links,
        "linksDropDownList"
    );

});