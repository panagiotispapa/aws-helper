package com.panos.awshelper;

import com.amazonaws.services.sns.model.Subscription;
import org.junit.Test;

import static com.panos.awshelper.Utils.*;
import static org.assertj.core.api.Assertions.assertThat;

public class UtilsTest {

    @Test
    public void canExtractQueueNameFromSubEndpoint() throws Exception {
        assertThat(extractSqsQueueName("arn:aws:sqs:eu-west-1:213441323650:queue1")).isEqualTo("queue1");
    }

    @Test
    public void canExtractRegionFromSubEndpoint() throws Exception {
        assertThat(extractSqsRegion("arn:aws:sqs:eu-west-1:213441323650:queue1")).isEqualTo("eu-west-1");
    }

    @Test
    public void canBuildQueueUrl() throws Exception {
        Subscription subscription = new Subscription().withEndpoint("arn:aws:sqs:eu-west-1:213441323650:queue1").withOwner("213441323650");
        assertThat(buildQueueUrl(subscription))
                .isEqualTo("https://sqs.eu-west-1.amazonaws.com/213441323650/queue1");
    }

}